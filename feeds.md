# Feeds

Atom feeds and RSS feeds are standards that made it convenient for users to 
subscribe to contents that offer them. I use atom feeds to get notified about
activities of other githubers/gitlabers in my browser. Here are the feeds
I follow.

- [Edvin](https://gitlab.com/edvinbasil.atom)
- [Huraken](https://sal.airno.de/feedbox/huraken.atom)

## Tips
To get github feeds for a username, use the following command  
```
curl -H "Accept: application/atom+xml" https://github.com/username > username.atom
```
The command will generate an atom feed `username.atom`. I serve such feeds
in a static folder and call them in feed agregators like 
[newsboat](https://wiki.archlinux.org/index.php/Newsboat)

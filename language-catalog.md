# Language Catalog
  
List of computer languages I use for various purposes.
  
## Latex
  
Represent math notations in code
  
[See full symbols List 🔗](https://oeis.org/wiki/List_of_LaTeX_mathematical_symbols)  
[Horizontal spacing reference 🔗](https://tex.stackexchange.com/questions/74353/what-commands-are-there-for-horizontal-spacing) 
  
## Regex
  
Represent text patterns and use it to match and capture parts of text.
  
[Try out regex here](https://regexr.com/)
  
## Nodejs
  
Cross-platform JavaScript runtime environment. Benefit from various packages useful for scripting, server-side programming, and front-end development + build tooling.  
  
### Quick links
- [Specifying versions in package.json](https://nodejs.dev/learn/the-package-json-guide#package-versions)
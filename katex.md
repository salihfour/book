# Notations

[See full symbols List 🔗](https://oeis.org/wiki/List_of_LaTeX_mathematical_symbols)  
[Horizontal spacing reference 🔗](https://tex.stackexchange.com/questions/74353/what-commands-are-there-for-horizontal-spacing)  
  
| Description               | Notation          | Rendered          |
|---------------------------|-------------------|-------------------|
| Subscript                 | `y_{sub}`         | $y_{sub}$         |
| Superscript               |`y^{sup}`          |$y^{sup}$          |
| Line break                | `\\`              |                   |
| Fraction                  | `\frac{A}{B}`     | $\frac{A}{B}$     |
| Sum                       | `\sum_{i=1}^{n}`  | $\sum_{i=1}^{n}$  |
| Multiply                  | `\times`          | $\times$          |
| Product                   | `\prod`           | $\prod$           |
| Log                       | `\log_{a}b`       | $\log_{a}b$       |
| 2em space                 | `\qquad`          | $|\qquad|$        |


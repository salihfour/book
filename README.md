# Personal Wiki

Here I organise my notes for later reference. They are primarily intended for my own use, but you are welcome to read them. I got inspired to make this book when I came across personal wikis of other great authors like [Nikita Voloboev](https://wiki.nikitavoloboev.xyz/)

I used docsify to build this site. Docsify is a lightweight javascript library that renders markdown files. You can get started using docsify from their [quickstart guide](https://docsify.now.sh/quickstart)
  
> [!ATTENTION]
> Index in sidebar of this book is being restructured. You might find arrangement of index items incoherent.
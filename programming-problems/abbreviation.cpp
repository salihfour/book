#include <iostream>
using namespace std;

// Hackerrank submission
// https://www.hackerrank.com/challenges/abbr/submissions/code/177225286
//
// Sample Input
// ------------
// 10
// Pi
// P
// AfPZN
// APZNC
// LDJAN
// LJJM
// UMKFW
// UMKFW
// KXzQ
// K
// LIT
// LIT
// QYCH
// QYCH
// DFIQG
// DFIQG
// sYOCa
// YOCN
// JHMWY
// 
// Output
// ------
// YES
// NO
// NO
// YES
// NO
// YES
// YES
// YES
// NO
// NO

bool issmall(char c) {
    if (c >= 'a' && c <= 'z') {
        return true;
    } else {
        return false;
    }
}

char uppercase(char c) {
    return c + ('A' - 'a');
}

void Run() {
    string a;
    string b;
    cin >> a;
    cin >> b;
    
    int al = a.length() + 1;
    int bl = b.length() + 1;
    bool** match = new bool*[bl];
    for(int i=0;i<bl; i++) {
        match[i] = new bool[al];
    }

    match[0][0] = true;
    /* cout << match[0][0] << " "; */
    for (int j=1;j<al;j++) {
        if (issmall(a[j-1]) && match[0][j-1]) {
            match[0][j] = true;
        } else if (!issmall(a[j-1])) {
            match[0][j] = false;
        }
        /* cout << match[0][j] << " "; */
    }
    /* cout << endl; */

    for (int i=1;i<bl;i++) {
        for (int j=0;j<al;j++) {
            bool case1 = b[i-1] == a[j-1];
            bool case2 = issmall(a[j-1]) && uppercase(a[j-1]) != b[i-1];
            bool case3 = issmall(a[j-1]) && uppercase(a[j-1]) == b[i-1];
            if (case1) {
                match[i][j] = match[i-1][j-1];
            } else if (case2) {
                match[i][j] = match[i][j-1];
            } else if (case3) {
                match[i][j] = match[i][j-1] || match[i-1][j-1];
            } else {
                match[i][j] = false;
            }
            /* cout << match[i][j] << " "; */
        }
        /* cout << endl; */
    }
    if (match[bl-1][al-1]) {
        cout << "YES" << endl;
    } else {
        cout << "NO" << endl;
    }

    for (int i=0;i<bl;i++) {
        delete(match[i]);
    }
    delete(match);
}

int main() {
    int q;
    cin >> q;
    for (int i=0;i<q;i++) {
        Run();
    }
    return 0;
}

#include <iostream>
#include <stdlib.h>
using namespace std;

// Hackerrank Submission
// 25-08-2020
// https://www.hackerrank.com/contests/coders-cult-aug-2020/challenges/largest-forest-1/submissions/code/1325928932

// Sample input
// ------------
// 5
// TTTWW
// TWWTT
// TWWTT
// TWTTT
// WWTTT
// 
// Output
// ------
// 10

struct TreeNode {
    struct TreeNode* p;
    int size;
};

typedef struct TreeNode node;

node* findset(node* n) {
    node* p = n->p;
    if (p == n) {
        return p;
    } else {
        return findset(p);
    }
}

void unite(node* a, node* b) {
    node* ap = findset(a);
    node* bp = findset(b);
    if (ap != bp) {
        bp->p = ap;
        ap->size += bp->size;
    }
}

node* makeset() {
    node* newnode = new node;
    newnode->p = newnode;
    newnode->size = 1;
    return newnode;
}

node*** field;

int main() {
    int dim;
    cin >> dim;
    char buf;

    field = new node**[dim];
    for (int i=0;i<dim;i++) {
        field[i] = new node*[dim];
    }
    
    node* largest;
    for (int i=0;i<dim;i++) {
        for (int j=0;j<dim;j++) {
            cin >> buf;
            if (buf == 'T') {
                field[i][j] = makeset();
                int left = j-1;
                int top = i-1;
                if (left >= 0 && field[i][left] != NULL) {
                    unite(field[i][left], field[i][j]);
                }
                if (top >= 0 && field[top][j] != NULL) {
                    unite(field[top][j], field[i][j]);
                }
                node* rep = findset(field[i][j]);
                if (largest == NULL || largest->size < rep->size) {
                    largest = rep;
                }
            }
        }
    }
    
    if (largest != NULL) {
        cout << largest->size << endl;
    } else {
        cout << 0 << endl;
    }

    return 0;
}

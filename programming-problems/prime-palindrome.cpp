#include <iostream>
#include <string>
using namespace std;

// Hackerrank Submission
// 25-08-2020
// https://www.hackerrank.com/contests/coders-cult-aug-2020/challenges/palidromes-but-primes/submissions/code/1325932279
//
// Sample input
// ------------
// 132757232
//
// Output
// ------------
// 133020331

bool isprime(long n) {
    if (n%2 == 0) {
        return false;
    }
    if (n%3 == 0) {
        return false;
    }
    long lim = n/2;
    long i = 1;
    while (true) {
        long dl = 6*i + -1;
        long dr = 6*i + 1;
        if (dl > lim) {
            break;
        }
        if (n%dl == 0 || n%dr == 0) {
            return false;
        }
        i += 1;
    }
    return true;
}

bool ispalindrome(string num) {
    int lim = num.size()/2;
    for (int i=0; i<lim;i++) {
        if (num[0+i] != num[(num.size()-1)-i]) {
            return false;
        }
    }
    return true;
}

int main() {
    long num;
    cin >> num;
    long i=0;
    long ans;
    long base1, base2;

    for (long i=0;i<num;i++) {
        long b1 = 6*i -1;
        long b2 = 6*i + 1;
        if (b1 > num) {
            base1 = b1;
            base2 = b2;
            break;
        }
        if (b2 > num) {
            base1 = b2;
            base2 = 6*(i+1) - 1;
            break;
        }
    }

    while (true) {
        long nr = base1+6*i;
        long nl = base2+6*i;
        if (ispalindrome(to_string(nl)) && isprime(nl)) {
            ans = nl;
            break;
        }
        if (ispalindrome(to_string(nr)) && isprime(nr)) {
            ans = nr;
            break;
        }
        i += 1;
    } 
    cout << ans << endl;
    return 0;
}

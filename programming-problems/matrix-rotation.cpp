#include <iostream>
using namespace std;

// Hackerrank submission
// Problem: Matrix rotation algorithm
// Difficulty: Hard
// https://www.hackerrank.com/challenges/matrix-rotation-algo/submissions/code/177662437

int perimeter(int m, int n) {                                                      
    int oa = m*n;                                                                  
    int ia = m-2<=0||n-2<=0?0:(m-2)*(n-2);                                         
    return oa - ia;                                                                
}

void countup(int m, int n, int si, int sj, int *ti, int *tj, int r) {
    r = r%perimeter(m, n);
    while (r > 0) {
        bool right = *ti-si == 0 && *tj-sj < n-1;
        bool down = *ti-si < m-1 && *tj-sj == n-1;
        bool left = *ti-si == m-1 && *tj-sj > 0;
        bool up = *ti-si > 0 && *tj-sj == 0;
        if (right) {
            /* cout << "right" << "shift" << endl; */
            *tj = *tj + 1;
        } else if (down) {
            /* cout << "down" << "shift" << endl; */
            *ti = *ti + 1;
        } else if (left) {
            /* cout << "left" << "shift" << endl; */
            *tj = *tj - 1;
        } else if (up) {
            /* cout << "up" << "shift" << endl; */
            *ti = *ti - 1;
        }
        /* cout << r << " shifted" << endl; */
        r--;
    }
    
}

int main(){
    int m, n, r;
    cin >> m >> n >> r;
    int ** matr = new int*[m];
    int ** matrr = new int*[m];
    for (int i=0;i<m;i++) {
        matr[i] = new int[n];
        matrr[i] = new int[n];
    }
    for (int i=0;i<m;i++) {
        for (int j=0;j<n;j++) {
            cin >> matr[i][j];
        }
    }

    int mind = m<n?m:n;
    int lim = mind%2==0?mind/2:(mind+1)/2;

    int malt = m, nalt = n;
    for (int cn = 0; cn<lim;cn++) {
        /* cout << "New circle" << endl; */
        int si = cn, sj=cn;
        int i = cn, j= cn;
        int ir = i, jr = j;
        countup(malt, nalt, si, sj, &ir, &jr, r);
        for (int l = perimeter(malt,nalt), t= 0; t<l; t++) {
            matrr[i][j] = matr[ir][jr];
            /* cout << i << " " << j << "\t" << ir << " " << jr << endl; */
            countup(malt, nalt, si, sj, &i, &j, 1);
            countup(malt, nalt, si, sj, &ir, &jr, 1);
        }
        malt = malt - 2;
        nalt = nalt - 2;
    }

    /* cout << "Answer\n"; */
    for (int i = 0;i<m;i++) {
        for (int j=0;j<n;j++) {
            cout << matrr[i][j] << " ";
        }
        cout << endl;
    }

    return 0;
}


#include <iostream>
using namespace std;

// Hackerrank submission
// https://www.hackerrank.com/contests/coders-cult-aug-2020/challenges/sum-of-subarray-1/submissions/code/1326110090
//
// Sample Input
// ------------
// 5 6
// 23 2 4 6 7
//
// Output
// ------
// True

int main() {
    long int n, k;
    cin >> n;
    cin >> k;
    long int * arr = new long int[n];
    for (int i=0;i<n;i++) {
        cin >> arr[i];
    }

    long int * psums, *sums;
    psums = arr;
    for (int n_ = n-1; n_ > 0; n_--) {
       sums = new long int[n_];
       for (int i=0;i<n_;i++) {
           sums[i] = psums[i] + arr[n-n_];
           bool case1 = k != 0 && sums[i] != 0 && sums[i] % k == 0;
           bool case2 = k == 0 && sums[i] == 0;
           if (case1 || case2) {
               cout << "True" << endl;
               return 0;
           }
       }
       psums = sums;
    }
    cout << "False" << endl;
    return 0;
}

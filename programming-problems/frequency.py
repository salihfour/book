#!/bin/python3

# Hackerrank submission
# https://www.hackerrank.com/challenges/frequency-queries/submissions/code/180246967?h_l=interview&playlist_slugs%5B%5D=interview-preparation-kit&playlist_slugs%5B%5D=dictionaries-hashmaps

# Sample test case
# ----------------
# 10
# 1 3
# 2 3
# 3 2
# 1 4
# 1 5
# 1 5
# 1 4
# 3 2
# 2 4
# 3 2

# Output
# ------
# 0
# 1
# 1

import math
import os
import random
import re
import sys

def decr(num):
    return num-1 if num > 0 else 0

# Data structure
freqs = {}
freqfreqs = {}

def get(key, dic):
    if key in dic:
        return dic[key]
    else:
        return 0

def incr(key, dic):
    if key in dic:
        dic[key] += 1
    else:
        dic[key] = 1

def decr(key, dic):
    if key in dic:
        dic[key] = dic[key]-1 if dic[key] > 1 else 0

def insert(val):
    oldfreqs = get(val, freqs)
    incr(val, freqs)
    newfreqs = get(val, freqs)
    decr(oldfreqs, freqfreqs)
    incr(newfreqs, freqfreqs)

def remove(val):
    oldfreqs = get(val, freqs)
    decr(val, freqs)
    newfreqs = get(val, freqs)
    decr(oldfreqs, freqfreqs)
    incr(newfreqs, freqfreqs)

def find(fr):
    return get(fr, freqfreqs)

# Completed freqQuery function 
def freqQuery(queries):
    outs = list()
    for i in queries:
        com = i[0]
        val = i[1];
        if com == 1:
            insert(val)
        elif com == 2:
            remove(val)
        elif com == 3:
            outs.append(1 if find(val) > 0 else 0)
    return outs

if __name__ == '__main__':
    fptr = open(os.environ['OUTPUT_PATH'], 'w')

    q = int(input().strip())

    queries = []

    for _ in range(q):
        queries.append(list(map(int, input().rstrip().split())))

    ans = freqQuery(queries)

    fptr.write('\n'.join(map(str, ans)))
    fptr.write('\n')

    fptr.close()

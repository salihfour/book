# Helpful techniques
- Disjoint sets using tree - CLRS Textbook
- Checking a given number is prime [https://www.geeksforgeeks.org/python-program-to-check-whether-a-number-is-prime-or-not/](https://www.geeksforgeeks.org/python-program-to-check-whether-a-number-is-prime-or-not/)

# Helpful c++ libraries
- iostream
- string

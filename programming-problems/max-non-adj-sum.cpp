#include <iostream>
using namespace std;

// Hackerrank submission
// https://www.hackerrank.com/challenges/max-array-sum/submissions/code/177141350
//
// Sample input
// ------------
// 5
// 3 7 4 6 5
//
// Output
// ------
// 13

// Space complexity: Constant
// Time complexity: O(n)

int max(int a, int b) {
    return a>b?a:b;
}

int main() {
    int n;
    cin >> n;
    int optjminus1 = 0;
    int optjminus2 = 0;
    int optj;
    for(int j=0;j<n;j++) {
        int a;
        cin >> a;
        optj = max(optjminus1, optjminus2+a);
        optjminus2 = optjminus1;
        optjminus1 = optj;
    }
    cout << optj << endl;
        
    return 0;
}

# Useful Tools

> A record of tools that I found useful at times.

## Homelab

- Samba, create Network Attached Storage (NAS) on Linux, [refer blog post](https://howtoraspberrypi.com/create-a-nas-with-your-raspberry-pi-and-samba/)
- Jellyfin
- Pihole
- Nginx
- Cloudflared, encrypt DNS queries

## Network

- Wireguard, virtual LAN

## Tui/Cli/Gui Tools

- Tmux
- Neovim
- Newsboat, command line feed reader
- Rtorrent
- Certbot, obtain SSL certificates for free
- Tar, archiver
- Rclone, connect to cloud storage providers
- Rsync
- Gnupg, pgp client
- OBS Studio, broadcasting studio
- Kdenlive, video editor
- Aria2 RPC with Web UI, refer [blog post](https://www.junian.net/tech/raspberry-pi-aria2-download-manager/)

## DevOps

- `entr`, lighweight linux cli for watching files
- Docker & Compose
- Terraform, infrastructure as code

## Platforms

- Heroku
- Netlify
- Azure
- AWS

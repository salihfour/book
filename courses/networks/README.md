# Switching<sup>[1]</sup>
## Circuit switching
Conceptually, when you or your computer places a telephone call, the switch-
ing equipment within the telephone system seeks out a physical path all the way
from your telephone to the receiver’s telephone. This technique is called circuit switching.<sup>[1]</sup>

- No conjestion
- No delay other than transmission delay once connection is established
- Users where histoically charged for time and distance

## Packet switching
Routers store and forward packets on its way to the destination. No dedicated
path is established. Packets might take different routes and arrive at 
destination out of order.
- Restricts size of packets to avoid monopolistic use of bandwidth
- processing and queueing delay present
- Efficiently use bandwidth
- Fault tolerant
- Users usually charged for volume or speed traffic
  
A tradeoff between circuit and packet switching is of guarantee and efficiency
  
# Internet History<sup>[2]</sup>
before 1960: _telegraph_, _telephone_, _constant rate communication_  
Later: _computer network_, _packet switched network_, _bursty data_  

The theory of packet switching for bursty traffic was first presented by Leonard
Kleinrock in 1961 at MIT. At the same time, two other researchers, Paul Baran at Rand
Institute and Donald Davies at National Physical Laboratory in England, published
some papers about packet-switched networks.<sup>[2]</sup>
  
## ARPANET
In the mid-1960s, mainframe computers in research organizations were stand-alone
devices. Computers from different manufacturers were unable to communicate with
one another. The Advanced Research Projects Agency (ARPA) in the Department of
Defense (DOD) was interested in finding a way to connect computers so that the
researchers they funded could share their findings, thereby reducing costs and eliminat-
ing duplication of effort.
In 1967, at an Association for Computing Machinery (ACM) meeting, ARPA pre-
sented its ideas for the Advanced Research Projects Agency Network (ARPANET),
a small network of connected computers. The idea was that each host computer (not
necessarily from the same manufacturer) would be attached to a specialized computer,
called an interface message processor (IMP). The IMPs, in turn, would be connected to
each other. Each IMP had to be able to communicate with other IMPs as well as with its
own attached host.
By 1969, ARPANET was a reality. Four nodes, at the University of California at
Los Angeles (UCLA), the University of California at Santa Barbara (UCSB), Stanford
Research Institute (SRI), and the University of Utah, were connected via the IMPs to
form a network. Software called the Network Control Protocol (NCP) provided com-
munication between the hosts.
  
## Birth of Internet
Cerf and Kahn (who were part of ARPANET team) in 1973 paper outlined a 
"TCP protocol". Responsibility for error correction was transferred from 
IMP to host machine. They wanted to link dissimilar networks so that host on 
one network can communicate with host on another. They used to call this 
the _Internetting Project_. In 1977, internet consisting of 
three networks, which includes ARPANET was successfully demonstrated.
Shortly after, TCP was split into two protocols TCP and IP which together became
known as TCP/IP.
  
## Internet Today
- World wide web _invented at cern by Tim Berners-Lee_
- VoIP, Video over IP, Television over IP (PPLive)
- Peer to Peer applications
  
## The TCP/IP Reference Model
TCP/IP model is a precursor to todays OSI model for network. It had
layers as described below.
  
Link layer: Least documented. An interface between hosts and transmission links
  
Internet layer: _deliver ip packets_, _ip protocol_, _icmp protocol_
  
ICMP protocol - Internet control message protocol
  
Transport layer: TCP delivers a bytestream originating on one computer 
without error to another computer. _tcp_, _segmentation of bytestream_. 
TCP also handles flow control to avoid swambing at slow receivers. UDP is
an unreliable connectionless protocol suitable for audio, video transmission.
  
Application layer: Contained high level protocols such as FTP, TELNET, SMTP.
  
## Formulas
### Maximum data rate (MDR)
1. For noiseless channel
$$ 2 \times B \times \log_{2}V\qquad bits/sec $$ 
  
2. For noisy channel
$$ B \times \log_{2}(1+\frac{S}{N})\qquad bits/sec $$
F - Bandwidth  
V - # of signal states  
$ \frac{S}{N} $ - signal to noise ratio  
$ Noise\ in\ decibels = 10 \log_{10}(\frac{S}{N}) $
  
## Glossary
__MAC:__ Media access control
  
# References
1. Tanenbaum Wetherall - Computer networks (2011)
2. Forouzan - Data Communications and Networking, 5th edition

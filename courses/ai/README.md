> 7 September 2020
# Intro

To pass turing test, computer should possess following capabilities
1. Natural language processing
2. Knowledge representation _store what is heard_
3. Automated reasoning _make conclusions_
4. Machine learning _adapt to new situations_
  
Total turing test has modifications that allow tester to pass items and test
perceptual capabilities. To pass it, a computer should also posses
5. Computer vision
6. Robotics
  
> 8 September 2020
# State of the art
  
![](./state-of-the-art.jpg)
  
# Agents
  
An agent is anything that can be viewed as
- perceiving its environment through sensors and
- acting upon that environment through actuators
  
Assumption: every agent can perceive its own actions (but not always the 
effects)

![](./agent.jpg)
  
## Human agent
- eyes, ears, and other as sensors
- hands, legs, mouth etc. as actutors
  
## Robotic agent
- cameras and infrared range finders as sensors
- various motors as actuators
  
## A software agent
- keystrokes, file contents, network packets etc. are sensed
- TODO (actuators)

# Agents and environments
  
Pecept: agent's perceptual input at any given moment
  
Percept sequence: complete history of everything the agent has ever perceived
  
An agent's choice of action at any given instant can depend on the entire
percept sequence observed to date
  
An agent's behavior is described by the **agent function** which maps from percept
histories to actions:
$$
[f:P^* \to A]
$$
  
We can imagine tabulating the agent function that describes any given agent
(_external charecterisation_)
  
Internally, the agent function will be implemented by an agent program which runs
on the physical architecture to produce $f$
  
# Vacuum-cleaner world
  
![](./vacuum-cleaner.jpg)
  
# Rational agents

An agent should strive to do the right thing, based on what it can perceive and
actions it can perform.
  
The right action is the one that will cause the agent to be most successful
  
Performance measure: An objective criterion for success of an agent's behavior.
Eg. amount of dirt cleaned up, amount of noise generated, amount of electricity
consumed etc. for vacuum cleaner agent.
  
As a general rule, it is better to design performance measures according
to what one actually wants in the environment. Rather than according to how 
one thinks the agent sould behave. For eg. use _cleanliness of floor_ instead
of _amount of dirt cleaned_.

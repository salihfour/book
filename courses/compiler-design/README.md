# Compiler Design

## Lecture notes for self study
- [Syntax directed translation_Part 1.pdf](./self-study-notes/Syntax%20directed%20translation_Part%201.md)
- [Syntax directed translation_Part 2.pdf](./self-study-notes/Syntax%20directed%20translation_Part%202.md)
- [Type systems_Part 1.pdf](./self-study-notes/Type%20systems_Part%201.md)
- [Type systems_Part 2.pdf](./self-study-notes/Type%20systems_Part%202.md)
- [Run time environments.pdf](./self-study-notes/Run-time%20environments.md)

## Grammars - Chomsky Hierarchy
- Type 0: Unrestricted
- Type 1: Context-sensitive
- Type 2: Context-free
- Type 3: Regular 
  
[Know more](https://www.geeksforgeeks.org/chomsky-hierarchy-in-theory-of-computation/)

## Context-free grammar
A context-free grammar can be represented by set of productions, each consisting of
1. A nonterminal called the head or left side of the production; this
production defines some of the strings denoted by the head.
2. The symbol $\rightarrow$ Sometimes $::=$ has been used in place of the arrow.
3. A body or right side consisting of zero or more terminals and non-terminals. 
The components of the body describe one way in which
strings of the non-terminal at the head can be constructed.

> If a grammar is context-free, then a symbol that is at the LHS of any
production must be a non-terminal. On the other hand, if a symbol is not 
found in the LHS of any production, it must be a terminal.

## Syntax directed definition
Syntax directed definition (SDD) is a context-free grammar together with attributes 
and rules. Attributes are associated with grammar symbols and rules are 
associated with productions.

Syntesized attributes of a non-terminal node are defined in terms of attributes
associated with itself and its children. Inherited attributes of non-terminal
node are defined in terms of attributes of itself, its parent and its siblings.
For attribute of terminals, value is supplied by lexical analyzer, and they are considered
to be synthesized. Terminals cannot have inherited attributes.

## Evaluating an SDD at the node of a parse tree
For a given node in a parse tree, if all attributes in it SDD are synthesized, values of
attributes can be evaluated by any bottom up traversal. (Eg. postorder). 
But if the SDD has both synthesized and inherited attributes, there is
no guarantee that the values can be evaluated in some order, as there
could be cyclical dependency.

## Dependency graph
If computation of the value of an attribute `b` in a parse tree depends on another attribute `c`, 
a directed edge can be drawn from `c` to `b`. A graph constructed in such a
manner is the dependency graph, which will help in determining an
order of evaluating values.

## Type checking
Type checking predict runtime errors at compile time. Ensures type of operands match the
operator.  
Types also have applications in determining storage needed at runtime, 
calculating address of array reference, choose the right operator etc.

### Type expression
Basic types of a language typically include boolean, char, integer, float, void.
  
A type name is a type expression
  
A type expression can be formed by 
1. applying array type constructor to a number and a type expression.
2. applying record type constructor to field names and their types.
3. using $\rightarrow$ for function types.
4. product of two type expressions
5. variables whose values are type expressions

### Type equivalance
Two type expressions are _structurally equivalant_ if and only if one of the 
following conditions is true
1. They are the same basic type
2. They are formed by applying the same constructor to structurally equivalant
types
3. One is a type that denotes the other
  
If type names are treated as standing for themselves, then first two conditions
lead to name _equivalance of type_ expressions

### Declarations
Types and declarations can be studied using a simplified grammar that declares
just one name at a time.

### Storage layout for local names
At compile time, the amount of storage known from types are used to assign
relative address for each name. Strings and dynamic arrays are reserved a known 
fixed amount of storage for a pointer to its data.
  
> Address alignment: charecters or strings will be allocated bytes in multiples
of four so that integers are all aligned at addresses that are divisible by
four. Such extra unused space resulting from this alignment is called padding.
  
Storage for names are computed by syntax directed translation
  
### Sequence of declarations
Non-terminals generating $\epsilon$, called __marker nonterminals__, can be used to
rewrite productions so that all actions appear at ends of right sides;
Eg. $$P \to \{offset=0;\} \hspace{1cm} D$$ can be written as
$$
P \to MD \\
M \to \epsilon \hspace{1cm} \{offset = 0;\}
$$

### Fields and records
A record type constructor has the form $record(t)$ where $record$ is the type
constructor and $t$ is a symbol table object that holds information about the
fields of this record type. It should be ensured that
1. field names within record must be distinct
2. offset for a field is relative to the data area for its record
  
#### Handling of field names in records
![fig](./translating-types-of-records.png "Handling of field names in records")

#### Type synthesis
![fig](./type-synthesis.png)

#### Type inference
![fig](./type-inference.png)

# Runtime environments
### Storage organisation
![](./storage-organisation.png)
#### Static vs dynamic storage allocation
_Static storage allocation_ - Storage locations computed at compile time
_Dynamic storage allocation_ - Storage locations computed at run time.
Dynamic storage is allocated in two strategies  
1. Stack - Usualy names local to a procedure
2. Heap - Data that outlives call to a procedure that created it
  
_Garbage collection_ supports heap management at run time to dete t useless data
elements and reuse their storage.
  
### Activation records
Procedure calls and returns are usually managed by _control stack_ where each
live activation has an _activation record_. The root of activation tree is at
the bottom and entire sequence of activation records on the stack correspond to
the path in the _activation tree_ to the activation where control corrently
resides.
![](./activation-record.png)
_it is assumed that stack grows downwards_
  
- Temporary values arise from evaluation of expressions
- Local data belong to the procedure
- Saved machine status include return address, and register state before 
procedure call.
- Control link points to activation record of caller
- Return values and parameters are usually stored in registers unless
they are big.

### Caller and Callee
1. Values communicated b/w caller and callee are generally placed at beginning
of the callee's activation record, so they are close as possible to caller.
Caller can place parameters there without creating callee's activation record.
2. Fixed length items like control link, access link, machine status are better
placed in the middle of activation record.
3. Other variable length items are placed towards the end of activation record.
4. A common approach to place top of stack pointer is after the fixed length 
fields, so that the callee procedure can access them by known negetive offsets.
Variable length items have to be accessed by positive offsets.
![](./caller-callee.png)
![](./calling-sequence.png)

# Run time environments
## Access to non-local data without nested proceedures
Example - C language  
Global variables are allocated static storage which can be accessed anywhere
using statically determined address  
Any other name must be local to the activation record at the top of the stack  

## Access issue with nested procedures
Access link of a method's activation function points to recent activation
record the method which is it's immediate parent in the lexical nesting
structure.  

Lexical nesting structure is the nesting structure in source code  
  
### Case 1
When callee is deeper than caller, the callee method definition has to
be immediately nested in caller's method definition. Otherwise this method
call would not have happened.  
Eg.
```
func a:
    ...
    func b:
        ...
    # a calls b
    b()
```
Access link of b will refer to caller's activation record.  

### Case 2
When callee is not deeper than caller, caller's definition has to be
nested at some depth within the definition where callee is immediately
nested. Otherwise the call would not have happened.  
Eg.
```
func a:
    ...

    func b:
        ...
        func b1:
            # b1 calls c
            c()

    func c:
        ...
```
nesting depths:  
$n_{b1} = 3$  
$n_{c} = 2$  
Access link of c should point to activation record of a, it can be
found by travelling $n_{b1} - (n_{c} - 1) = 2$ hops in the access link starting from b1.

> Scope nesting will enable inner functions to call outer functions, but
will not allow outer functions to call inner functions unless the inner
function is immediately nested i.e within the local scope of outer function.

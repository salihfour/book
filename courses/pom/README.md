# Principles of Management

[Materials for self study](https://drive.google.com/drive/folders/16LT8y2Yv8mdP_vlVzUWHZ8S242lpoqE_?usp=sharing)
  
## Decision Theory
[Video lecture](https://www.youtube.com/playlist?list=PLPEQA16isHAFH4PhR0LfFsGqYI1g2YzcP) 
  
Selecting the _best alternative_ from a set of available options so as 
to help decision maker move towards the desired objective  
Certain decisions affect a large number of succeeding decisions, come under
the category of multi-stage decision making. Otherwise, it is single stage.
Eg. Going for lunch vs Choosing a degree

### Decision making
- Certainty: All data available
- Risk: Some data available
- Uncertainty: Negligible data is available, losts of assumption
  
Ambiguity and chances of bad decision increases as uncertainty increases

#### Under certainty
Examples
- Buying a product after enquiring prices from two or three shops.
- Linear programming problems
- Transportation problems
- Decision does not depend on personal traints of decision maker  
  
#### Under risk
Following criterias will be studied
1. Maximum likelihood principle
2. Expectation principle | expected monetary value criterion
3. Expected opportunity loss | expected regret
  
Following related terms will be understood
1. Expected payoff of perfect information (EPPI)
2. Expected value of perfect information (EVPI)
  
#### Under uncertainty
Following criterions will be studied
1. Laplace principle | Principle of insufficient reason | Principle of equal
likelihood | Criterion of rationality
2. Maxmin | Minmax | Criterion of pessimism
3. Maximax | Minimin | Criterion of optimism
4. Hurwics Principle | Criterion of realism
5. Savage principle | Minimax regret criterion
  
As uncertainty increases, human influence in decision making increases, that
explains why there are so many criterion for decision making under 
risk / uncertainty.

## Single stage decision making
[Video tutorial](https://youtu.be/txu9FCOJ05w)
  
No long sequence of interrelated decisions to make
### Fundamental terms
1. **Course of action | Acts | Activities | Strategies | Decision**:  
Within the control of decision maker
2. **States of nature | Events | Possible outcomes:**
Beyond the control of decision maker
3. **Payoff function | Payoff value | Payoff matrix:**
Consequences resulting from various combinations of acts & events
  
![](./terms-illustration.png)

### An example problem solved in steps
- [Watch on Youtube](https://youtu.be/txu9FCOJ05w?t=367)
  
Laplace criterion assumes all scenarios are equally likely, finds arithmetic
mean of payoff values in each case and takes the highest value / lowest cost
decision
  
Maximin is profit based. Minimax is Cost based. Both minimax and maximin are 
adopted by extreme pessimists. Maximin takes a decision corresponding to 
maximum profits by treating any decision will give only the minimum profit 
possble. Minimax selects the decision that gives the minimum loss considering 
any decision will give its maximum losses possible.
  
Maximax takes the best profiting decision considering all decision will give
its maximum profit possible. Similarly, minimin takes the least loss decision
considering all decisions will generate only the least loss possible. These decisions
are optimistic.



Hurwics principle leads to a decision that is somewhere between optimism
and pessimism, denoted by $\alpha$ the value of optimism. Payoff value for each 
decision is calculated as follows...   
  
![](./hurwics-payoff.png)
  
When $\alpha$ is 1, the model imitates optimistic criterion. When $\alpha$ is
zero, the model imitates pessimistic criterion.
  
Savage principle constructs a regret table (opportunity loss), and finds maximum
of regrets for each decision to select the decision that gives minimum regret.

> Point to ponder: If you are looking to minimize regret, then why not select the course of action
that gives maximum payoff?
  
### Decision making under risk
Maximum likelihood estimation assumes maximum probability event to happen and 
the decision giving maximum payoff for that event is selected.
  
Expectation principle calculates expected payoff for each decision (considering
probabilities of event) and takes selects the decision that gives maximum expected payoff.
  
Expected opportunity loss criterion constructs regret table and calculates estimated
regret considering probabilities of events to finally select the decision with minimum
expected regret.
  
> Both expectation principle and expected regret leads to same decision.
  
### Terms under Risk
Expected payoff (EP)  
Expected cost (EC)  
Expected payoff of perfect information (EPPI)  
Expected cost of perfect information (ECPI)  
Expected value of perfect information (EVPI)  
  
_Perfect information_ is the one that is bought from third parties / companies, 
that which collect data.

$$
EVPI = EPPI - EP \\
EVPI = EC - ECPI
$$
  
EVPI is the maximum price a decision maker can pay for getting more information
without incurring losses.

## Multi stage decision making
Succeeding decisions depend on preceding decisions. This leads to branched structure,
and a tree representation is more helpful. In the visualisation, there will
be decision nodes, and chance nodes both creating branches. Branches at decision
nodes are taken by decision maker. At chance nodes, the direction of movement
is not at the hands of decision maker, instead it depends on external events. Decision
trees are solved in a backward direction (bottom up).
  
### Steps
1. Properly construct the decision tree
2. Compute expected payoff of each decision (from right to left) for each decision node.
Strike of rejected course of actions by looking at the expected payoffs (In case of tie
select any one course of action). Write down
the expected payoff of selected course of action inside each decision node. It will be used
to calculate expected payoff of the parental node.
3. When we reach at the root node, no decision nodes will have branches, i.e
all decisions are taken care of. But chance nodes will remain intact.
  
[Sample problem with explanation - watch on youtube](https://youtu.be/a89oLx64dts?t=449)
  
_Sneak peek from youtube_  
![](./tree-solving.png)

## Work study
Work study increase productivity. It involves  
- Method study
- Work measurement (time study)
  
Work study finds a safer, productive, quicker, easier way of doing a task
  
### Three aspects
- Method
- Time
- Ergonomics
  
Method study comes before time study. Find the best method and do time study
for that method only.
  
### Standard Time
The time taken by an avg experienced worker for a task with provisions for
delays beyond the worker's control (allowances)
  
Standard time helps in decision making, entering contracts, understand
production rate and delivery time.  

$$
NormalTime = ObservedTime * PerformanceRatingFactor \\
\ \\
StandardTime = NormalTime + Allowances \\
\ \\
StandardTime = ObservedTime * RatingFactor(1 + Allowances) \\
\ \\
StandardTime = NormalTime(1 + allowances)
$$
  
Normal time is the time taken by a qualified worker without bonus motivational
factors. Normal time assumes continueous work, but standard time has allowances for breaks.
  
$$
PerformanceRating\ or\ RatingFactor = \frac{ObservedRating}{StandardRating}
$$  
_Standard rating is considered 100_
  
> Rating factor is based entirely on time study analysts experience and is subjective.
  
## Forecasting methods
### Qualitative
- Delphi technique
- Market research
- History analogy
- Sales force opinion
- Jury of executive opinion
  
### Quantitative
1. Time series model
    - Simple average
    $$
    F_{n+1} = \frac{\sum_{i} D_{i}}{n}
    $$
    - Weighted avg: Each $D_{i}$ in simple average will have a wieght
    - Moving average:  
    Eg. with 3 unit window
    $$ 
    F_{4} = \frac{D_{1} + D_{2} + D_{3}}{3}
    $$
    - Weighted moving avg: Each $D_{i}$ in simple moving avg will have a weight
    - Exponential smoothing:  
    Only method which takes care of forecasting error.  
    $\alpha$ is the smoothing constant, by default it is taken as $\frac{2}{n+1}$ where $n$ is the number of demand data points.
      
    Eg. with months
    $$
    F_{feb} = F_{jan} + \alpha (D_{jan} - F_{jan})
    $$
    _difference of D and F is the forcasting error_  
    Formula  
    $$
    F_{t} = F_{t-1} + \alpha (D_{t-1} - F_{t-1}) \\
    \ \\
    which\ is\ equivalant\ to \\
    \ \\
    F_{t} = \alpha D_{t-1} + \alpha (1-\alpha) D_{t-2} + \alpha(1-\alpha)^2 D_{t-3} + \alpha (1-\alpha)^3 D_{t-1} + ... 
    $$

  
2. Casual series model
    - Linear trend regression
  
## Wage incentive plans
1. Straight piece rate
2. Straight piece rate with guaranteed min wage
    Two cases, when worker produces less than standard, he gets minimum wage for his hours of work as if he produced standard number of pieces for that many hours. If he produces equal to or more than standard number, then he gets wage according to straight piece rate.
3. Differential piece rate
    Piece rate depends on number of pieces produced
  
![](./piece-rates.png)  
4. Halsey plan
    If a worker complets a job faster, then he will get a percentage bonus of the time he saved.
    $$
    Wage = HourlyRate \times ActualTime + (P/100)\times(StdTime - ActualTime) \times HourlyRate
    $$
5. Rowan plan
    Similar to halsey plan, but fraction of time saved is taken as percentage bonus
    $$
    Wage = HourlyRate \times ActualTime + \frac{ActualTime}{StdTime} \times (StdTime - ActualTime) \times HourlyRate
    $$
6. Emerson's efficiency plan
  
![](./emersons.png)
![](./emersons-graph.png)

# Operations Management
Concerned with the transformation of a range of inputs into the required output (services)
with requisite quality.  

Joseph G .Monks defines Operations Management as the process whereby
resources, flowing within a defined system, are combined and transformed by
a controlled manner to add value in accordance with policies communicated
by management.
  
## Scope
- Location of facilities.
- Plant layouts and Material Handling.
- Product Design.
- Process Design.
- Production and Planning Control.
- Quality Control.
- Materials Management.
- Maintenance Management
  
## Objectives
1. Customer service
2. Resource utilisation
  
## Key components
1. Resources
2. Systems
3. Transformation

## Skills of operations manager
- Technical knowledge in the field
- Interpersonal
- Communication
- Strategic
- Process design and improvements
  
Should have knowledge of new trends
- Mass customization
- Supply chain Management
- Outsourcing
- Lean Manufacturing
- Agility
- E-Commerce
  
## Production vs Operation management
Production is part of operations management.
Operations is concerned with provision of services to customers as well.
Operation's scope include location, storage, logistics, inventory management etc.
along with that of product quality, design, quantity, cost etc.
Operations management can be found even in organisations where production
of goods is not undertaken.
  
## Consumer products
- Convenience products (Eg. soap)
- Shopping products (Eg. laptop)
- Speciality products (Eg. Rare artwork)
  
## Industrial products
- Machine / parts : used in manufacturing, include raw materials
- Capital items : Installations
- Business services and supplies: Human work involved in industry, other
supplies like stationary, lubricants
  
## Services
Has the following charecteristics  
- Intangibility
- Inconsistency
- Inseparability from the provider unlike products
- Not storable
  
### Types
1. Business
2. Personal
3. Social
  
# Relevant seminar reports for endsem exam
- [Google drive folder](https://drive.google.com/drive/folders/1faIoZyY_JPI4UmVDgFG84Bws9RAPR6zu?usp=sharing)

# Marketing
## Marketing Mix
4Ps - product, pricing, place, promotion  
4Cs - consumer, cost, convenience, communication
  
4Cs is a consumer centric version of 4Ps.
  
## Marketing decision making
Controllable elements
- Product variations
- Select of distribution channels
- Set Price
- Fix Budget for sales and promotion
Environmental elements
- Demand
- Competition
- Non marketing costs
- Structure of distribution / available distribution channels
- Public policy
  
## Marketing vs Selling
Marketing is a form of communication, by which value of a product is communicated.
Additionally, sales is a part of marketing. Sales is about transfer of 
ownership in exchange of profit.
  
Marketing is mainly concerned with customer satisfaction and it starts at customer. Consequently it
is innovative so as to provide better value to customers. It plans in the long term.
Whereas selling views customer as the last link and is concerned with profit maximization. Selling usually stick with existing technology
to reduce cost. It's plans are short term.

Selling focus on sellers needs alone, marketing largely focus on customer needs.
  
Examples of Marketing oriented companies
- Dell
- Automobiles
- Designer clothes
  
Examples of selling oriented companies
- Online shopping
- Door to door selling
  
## Five marketing concepts
1. Production concept
Produce more so as to cut down production cost and selling price. Build profit through sales volume.
2. Product Concept
Improve quality, performance and features. Price is a lower priority. Assumes
sales will increase when quality increase.
3. Selling Concept
Aggressively promote products. Consumer satisfaction is secondary to sales.
4. Marketing Concpet
Consumer centric. Work backwards from consumers to products inorder to maximise
consumer satisfaction.
5. Societal Marketing
Balance of consumer satisfaction, society's well being and company profits.
  
## Marketing Management
Process of marketing management  
![](./marketing-management.png)
  
Approach / Direction of marketing  
![](./direction.png)
  
# Patents and Intellectual Property Rights
Intellectual property (IP) are creations of the mind. People protect them by
patents, copyright and trademark to earn recognition and financial benefit.
  
The rights of authors of literary and artistic works are protected by copyright
for a minimum period of 50 years after death of author.
  
## Industrial property
- Protection of distinctive signs that are used to distinguish goods and services
of one undertaking from other undertakings. Protection of geographical indications
that identify origin of goods. This provides a fair competitive platform and 
protect buyers from fabrications.
- Protection of innovations / inventions by patent. Aims to stimulate innovation
and industrial design / trade secrets. Patents are usually given for 20 years.
  
## Types of intellectual property
1. Patents
After conceptualisation and review of an idea, if the idea is meritorious enough
for a patent, it is recommended to work with a patent attorney to patend the invention.
Patent holders are responsible for tracking usage and timely renewal. They can
sell, licence, and donate patent.  
2. Trademarks
Protects symbols in a broad sense, which include words, smells, sound, color scheme
etc. It identifies a company. It need not be seperately registered, but is automatically
in effect through abuntant use in interstate commerce. Registration
provides concrete protection. 
3. Copyrights
Copyright protects expressions. They allow owner of a material to control its reproduction,
distribution, adaptations, etc. Copyrights are ineffect upon a material's dissemination,
 but registration provides optimal protection.
4. Trade Secrets
Secrets of a company that provides competitive advantages are supposed to be kept
confidential by the company itself. There is no federally-regulated registration
process. It's access has to be restricted and dissemination can be protected using non-disclosure 
agreements.

## Other terms
IP Insurance: An insurance that covers legal costs for pursuing theft of IP  
  
IP Contracts: Contracts executed among collaborating instututions for sale of
IP rights. Describes rights and responsibilities of collaborators, during the 
term of collaboration.
  
## Patent
“Patent, is a legal document granted by the government giving an inventor the exclusive
right to make, use, and sell an invention for a specified number of years. Patents are also
available for significant improvements on previously invented items”
  
Patent rights are territorial, Indian patent does not confer any rights outside
india. So they have to be applied for in each nation as needed.

## Patentability Conditions
1. Invention should not belong to non-patentable subject matters.
2. Invention must be novel
3. Invention should be non-obvious, i.e should have a significant improvement to
existing technology.
4. Invention should be feasible / have practical utility

## Non-patentable
According to Indian Law
1. Cannot patent a law of nature
2. Invention must relate to a substance that can be produced.
3. In medicine, and certain chemicals, patent is not granted for the substance.
But a process of manufacturing them is patentable.
4. Invention related to a specified set of radioactive elements are not patentable.
5. Formulation of an abstract theory, or invention that contradicts ethics / public welfare.
6. Mere discovery of new property of a known substance
7. Mere reassemply of known items
8. A method of agriculture or horticulture. A process of medical treatment.
9. Parts or whole of plants / animals or biological processes except relating to microorganisms.
  
## Types of patent
1. Utility
2. Design
3. Plant patent
Engineered plant verieties
4. Provisional Patent
A protection for up to twelve months inorder to develop an idea described
in a provisional application.
  
## Patent granting procedure
1. Patent searches
2. Patent drafting
3. Filing
4. Publication (After 18 months of filing, patent is publicised in a journal
5. Pre-grant opposition
6. Examination / Patent prosecution
7. Grant
8. Post grant opposition

# Financial Management
Finance includes anything in your name that has financial value. Money is a form
of finance.
  
## Types of financial management
1. Capital budgeting  
Ranking projects based on future returns so that company can decide which to
invest in first.
2. Capital structure  
Equity includes captal, reserves, retained earnings. Debts are loans / deposits.
Equity and debt together are funds.
3. Working capital management  
Ensures that company offers efficiently. Monitors assets and liabilities.
$$
Current ratio = \frac{Current assets}{Current liabilities} 
$$
  
## Financial Manager
Financial managers are responsible for the financial health of an
organization. They produce financial reports, direct investment activities, and
develop strategies and plans for the long-term financial goals of their
organization.
  
Goal of manager is to maximise corporation's profits.
  
Financial manager focus on long-term strategy, profit margins, investment options
captal acquiry etc. Accounting / bookkeeping works are done by his
subordinates. He will manage proposals, bidding process etc. Cash flow might
differ from ideal because payments could become late. 
Finance manager should adequetely reserve credit and cash
so as to keep the company stable. He also should make the business finance in
compliance with legal obligations, by working with tax experts and CPAs.
  
## Functions
- Estimation of capital needs
_fixed capital_, _working capital_
- Selecting the sources of funds
_terms and conditions_
- Financial Analysis and Interpretation
_financial position_,
_investigate potential_, _determine profitability_,
_estimate ability_, _concern solvency_, _examine earning capacity_ 
- Investment of Funds
_wisely improving fixed assets_
- Working Capital Management
- Profit planning and Control
- Deciding Dividend Policies

## Financial institutions (FI)
_fractional reserve banking_, 
### Functions
1. Liability-asset transformation: Bank liabilities (savings) are transformed to
bank assets (loans).
2. Size transformation: Small deposits are pooled for large loans
3. Risk transformation: Risk of default minimized via techniques like diversification
4. Maturity transformation: Short term deposits are pooled for long term loans.
  
## Types of FIs
1. Central banks
2. Retail / Commercial banks
3. Internet banks (retail / commercial)
4. Credit unions
5. Savings and loan associations (Not many nowadays)
6. Investment banks / companies (mutual fund)
7. Brokerage firms (stock trade)
8. Insurance
7. Mortgage companies
  
# Human Resource Management (HRM)
Staffing is the function of appropriately filling vacant positions with the 
right personnel at the right time. Training improves a person's ability at a 
particular job. 
  
Recruitment generates a pool of qualified applicants for organizational jobs.
It starts with advertising jobs and applications are invited positively.
Afterwards, a negetive approach of shortlisting a limited number candidates from the pool 
in order to fill the vacancy is performed. This process is called selection.
Selection might come in stages, where each stage generates a shortlist that
will be passed on to next stage.
  
External recruiting: _college/university_, _labor unions_, _employment agencies_,
_competitive sources_, _job fairs_  
Internal recruiting: _employee databases_, _promotions/transfers_, _job posting_,
_referrals_, _re-recruiting of former employees_  
  
Internet recruitment: _career sites_, _job boards_
  
## Recruiting evaluation metrics
- Quantity
- Quality
- Time req to fill openings
- Cost
- Satisfaction
  
## Selection process  
![](./selection-process.png)
  
> Hire hard, manage easy
  
When the right people with appropriate capabilities are selected, management
becomes easier.
  
## Selection testing
- Ability tests
- Personality tests
- Honesty/Integrity tests
  
## Interviews
  
### Structured  
_predetermiined questions_, _easy comparison of candidates_
1. Biographical  
_chronological assessment_
2. Behavioral
_ask how a situation was handled in the past_
3. Competency interview
_questions reveal skills relevant to job_
4. Situational
_hypothetical situation_, _how to handle_
  
### Semi structured

_has predetermined questions_, _discussion sprouts new questions_

### Unstructured

_spontaneous_, _free flowing questions_

## How to conduct?

1. Panel interview: _several interviewers_, _one candidate_
2. Team interview: Interviewers are the team to which intervewee will work with if
he gets selected.

## Stress interview

_put pressure_, _aggressive/insulting posture_, _for high stress jobs_

## Exit interview

When an employee leaves an organisation, valuable feedback is gathered. This will
help to improve retention of employees in future.

## Training and development

Managers enhance their leadership skills, employees gain skills and adapt to changes in organisation.

1. Hard skills: _resources usage_
2. Soft skills: _communication_, _mentoring_
  
### Training components

- Is there really a need for the training?
- Who needs to be trained?
- Who will do the training?
- What form will the training take?
- How will knowledge be transferred to the job?
- How will the training be evaluated?

### Training process

![](./training-process.png)
  
### Training needs assesment - sources

![](./needs-assesment.png)
  
### Training Objectives

1. Knowledge
2. Skill
3. Attitude

### Training design

- Determining learner readiness
- Understanding different learning styles
- Designing training for transfer.
  
### Training delivery

Schedule, conduct, monitor
  
![](./delivery.png)
  
### Training evaluation
Compares results with objectives, primarily because training is costly and its
effects has to be analyzed.  
Evaluations may be done at four levels
1. Reaction
2. Learning
3. Behavior
4. Results

## Development

Improve employees beyond short term requirements. They will be an asset to
organisation. Employees will benefit as well.
  
Development approaches  
![](./dev-approaches.png)
  
Devleopment is important for managers as well. Pre-supervisor training is common.
Some mistakes in development efforts are  
- Failing to conduct adequate needs analysis
- Trying our fad programs or training methods
- Substituting training instead of selecting qualified individuals.
  
## Methods of training
1. Training for operatives
    - On the job training: _inexpensive_
        1. Vestibule training: _seperate workplace for trainees_, _reduced risk from error_, _relatively costly_
        2. Apprenticeship: _classroom_, _no guarantee of effect_, _provide thoery and experience_
        3. Internship
    - Simulation: _expensive_
    - Instruction: Subject matter is devided into frames. Employee can master each
    frame at their own pace. Frames might have linear dependencies and branching among them.
2. For managers
    - Observation assignment: _understudy_, _assistant_, _perpetuate existing practice_,
    _mistakes perpetuate as well_
    - Position rotation: _understand interdepartmental relations_, _broaden experience_
    - Serving on committees: _one in a committee_, _learn from seniors_, _adjust to enterprise needs_
    - Assignment of special project: _hands on experience_

## Types of training
1. Technical
2. Quality
3. Skills
4. Soft skills
5. Professional training and Legal training
6. Team training
7. Managerial training
8. Safety training

# Logistic Regression
- Prediction is in the interval [0,1]
- $ h_\theta(x) = g(\theta^{T}x) $ where $ g(z) = \frac{1}{1+e^{-z}} $
- $ g(z) $ here is the sigmoid function  
![](./sigmoid.png)
- Prediction indicates probability of the label being 1
- $ \theta^{T}x $ is linear. It can be replaced by non linear polynomials to get
non linear decision boundaries
- $ Cost\ function\ Cost(h_{\theta}(x), y) = - SUM\ where\ SUM = (y)log(h_{\theta}(x)) + (1-y)log(1-h_{\theta}(x)) $
- $ Total\ cost = \frac{1}{m_{test}}\sum_{i=1}^{m_{test}}Cost(h_{\theta}(x^{(i)}), y^{(i)}) $
- Misclassification error _an alternate cost function_  
![](./misclass-error.png)
# Neural networks
- Hyperparameters are set by us. Parameters are learned through training
- Forward propagation  
![](./forward-prop.png)
- Parameter dimensions  
![](./parameter-dimensions.png)
- Backpropagation
![](./backprop.png)
- Backpropagation equations
![](./backprop-eqs.png)
- Backprop initialization
![](./backprop-init.png)

# Advice for applying ML
## Linear regression
- Randomise data before splitting it into train and test
## Evaluating multiple hypothesis
- Split data into 3 sets namely training, cross validation, and test
- Spliting data into these 3 sets gives us 3 variants of error respectively
- Use training error to fit data. Use cross validation error to compare hypotheses,
choose one hypothesis and use test error to estimate its generalization error.
- Graph showing relationship between higher degree polynomial hypothesis and the
errors they give after fitting  
![](./bias-variance.png)
## Evaluating regularisation parameters
![](./regu1.png)
![](./regu2.png)
![](./bias-var-regu.png)
## Bias vs Variance
- In high bias problem, increasing data does not help
- In high variance situation, increasing data does help
![](./high-bias.png)  
![](./high-var.png)  
## Debugging a learning algorithm
![](./debugging.png)
## Neural networks and overfitting
- Small network - fewer parameters - prone to underfitting
- Large neural network - more parameters - prone to overfitting
- Use regularisation to address overfitting in large networks

# Confusion Matrix
- **true positive (TP):** eqv. with hit
- **true negative (TN):** eqv. with correct rejection
- **false positive (FP):** eqv. with false alarm, Type I error
- **false negative (FN):** eqv. with miss, Type II error
![](./conf-matrix.png)  
Ex. with more classes  
![](./multiclass-conf-matrix.png)
  
- Accuracy is $ \frac{TP}{m} $ where $ m $ is number of data rows
- Precision is $ \frac{TP}{TP + FP} $ for a given class _compute from column_
- Avg precision is $ \frac{\sum_{classes} Precision}{No. of classes} $
- Recall is $ \frac{TP}{TP + FN} $ for a given class _compute from row_
- Avg recall is $ \frac{sum_{classes} Recall}{No. of classes} $
- $ F1\ Score = 2 \times \frac{Precision \times Recall}{Precision + Recall} $ 
(Avg. precision and recall is used here)

- Accuracy is a bad metric on imbalanced data, whereas F1 score is a good metric.

# Naive bayes
Probability that the true label is of class $C_k$ given input $(x_1, x_2 .., x_n)$    
![](./bayes-class-prob.png)  
Classifier  
![](./naive-bayes.png)  

# SVM
Aim is to minimize cost as given below
![](./svm-prob.png)  
SVM is considered as a large margin classifier  


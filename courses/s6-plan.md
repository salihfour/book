# Semester VI course plan

[Minutes](https://drive.google.com/drive/folders/16DZgRpBB_lSXq7WfT3tAdqvNsX1uKXyU?usp=sharing)

## Machine Learning
- Group presentation via zoom with PPT and report - 7th july
- [Individual assignment and paper presentation](https://drive.google.com/file/d/166iHiIviGgW7fuCsnQGPl_xrjH7kn-XU/view?usp=sharing)  
  [Fill in a topic here](https://docs.google.com/spreadsheets/d/1ek2tK5-pI2dizPNwWhjVfiu1l8NGOlB8Ry9L7Jz_jJc/edit?usp=sharing)


## Compiler Design
- [Quizes in google classrom](https://classroom.google.com/)
- [Materials in Eduserver](https://eduserver.cse.nitc.ac.in/)

## Computer Networks
- Demonstration of network utility (30 marks)
- Prerecorded seminar (20 marks). Choose any topic related to transport and network layer, show ppt with narration. 5 - 10 mnts, save to dropbox folder 'PRS'
- Assignment (20 marks)
- No exam

## Software engineering
- No idea

## POM
- [Youtube lectures](https://www.youtube.com/playlist?list=PLPEQA16isHAFH4PhR0LfFsGqYI1g2YzcP)
- [PPT and solved problems](https://drive.google.com/drive/folders/16LT8y2Yv8mdP_vlVzUWHZ8S242lpoqE_?usp=sharing)

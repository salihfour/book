# Software Engineering

## Coding and Testing - Lecture Video 4

### Good coding practices

#### Goal of coding
Implement design in best possible manner.

#### Objectives
- Minimize effort
- Minimize number of statements
- Minimize memory
- Maximize clarity
- Maximize output clarity

#### Guidelines
As code gets larger, debugging becomes hard. To make maintenance easier,
programmers started structured programming, where how the code executes is similar
to how the code is written. We can get an idea of execution flow by 
looking at the code. Goto statements is an example that make it hard to maintain
code. The following are some good practices.
- Control construct should be single entry single exit.
- Use gotos sparringly
- Use information hiding
- Exploit user defined types
- Avoid deep nesting
- Large modules are not usually functionally cohesive
- Break complex things to multiple modules
- Avoid side effects
- Handle exceptions, avoid empty catch block
- Check read inputs
- Use default block in switch case

### Incrementally develop code
- Incremental development
- Test driven development
- Pair programming - one person codes while another reviews

### Managing and evolving code
- Source code control and build 
  
#### Refactoring
Techniques to improve existing code and prevent this design decay

##### Objective
- Reduced coupling
- Increased cohesion
- Better adherence to open-closed principle
  
##### Golden rules
- Refactor in small steps
- Have test scripts to test existing functionality

### Testing
  
#### Unit testing
Checking of code by programmer at programmer level, requires stubs or drivers to be created.
Drivers mock calling modules, and stubs mock called modules. Drivers and stubs are used
in place of actual modules for speed and isolated testing.

#### Code inspection
Static testing, defects detected through manual review.
Conducted by programmers for programmers. Structured process with defined roles for participants,
all focus on identifying the defect, not fixing it.

### Q&A
1. Which all statements are true about coding phase in SDLC (Systems development life cycle)
- [x] Affects both testing and maintenance
- [x] The goal is to implement the design in best possible manner
- [ ] Objective is to reduce implementation cost

2. Different criteria for judging a program  
- [x] Readability
- [x] Size
- [x] Execution time
- [x] Required memory

3. Structured programming is often regarded as goto less programming  
Ans. No, structured programming only says goto statements are not good. But if goto statement
makes program more readable, like when in error handling, it is encouraged to use it.

4. Benefit matching static structure to dynamic structure
- [ ] Faster execution
- [x] Easier to understand

5. Information hiding __ coupling  
Ans. Reduces  

6. In test driven development, code is in sync with tests  
Ans. True  

7. Refactoring is about  
Ans. Improving design of code that already exists  

## Software Testing - Lecture 5
### Goal
- Ensure quality of delivered software, remove defefts
- Two types - static (inspect code), dynamic (execute and identify)
- Expensive, can consume unlimited effort
- Verification: Are we building the project right
- Validation: Are we building the right project

### Error, fault, failure
- Error: Descripancy between observed outcome and expected outcome
- Fault: Condition that causes the system to fail
- Failure - Inability of a system to act out according to specification

### Psychology
- Testing identifies presense of faults, which are then identified by debugging
- We cant say anything about absence of faults if tests are successful, so 
intent of testing is to show that program does not work.

### Levels of Testing
1. Unit testing (of modules, by developer)
2. Integration testing (of interaction b/w modules, by developer and tester).
Drivers and stubs are implemented here.
3. System testing (of integrated system, by tester)
4. User acceptance testing (by end user)

### Regression testing
To ensure code change has not adversely affected existing features. It runs a
predefined test suite which was used before making modifications.

### Validation testing
Alpha: User comes to developers environment and test the software.
Beta: Test by users after software is released.

### Testing process
![](./testing-process.png)

### System testing
- Recovery (MTTR calculated). Cause failures and measure recovery
- Security 
- Stress
- Performance - Testing correctness and speed requirements

### Black box testing
![](./black-box-testing.png)

#### Equivalance class partitioning
Divide input domain into valid & invalid inputs and test each partition.

#### Boundary value testing
Select inputs of boundary cases, this complements equivalance partitioning.

#### Pairwise testing
Test combination of inputs that might result in faults. To test all combinations
are combinatorially hard. Instead, if we have n values, we can test one
value combining with each of the other n-1 values, and then discard that 
one value. This process are to be repeated with remaining values. Now this idea
can be extended to the case where each parameter has, say m values to be tested.
The complexity will be `m*m*n*(n-1)/2`.

### Q&A
1. The difference between testing and debugging is  
Ans. Testing finds presense of faults and debugging finds the fault.
2. In ....., test cases are designed using only the functional specification of the 
software without any knowledge of internal structure
Ans. Black box testing
3. ..... is sometimes performed with realistic data of the client to demonstrate
the software is working satisfactorily  
Ans. Acceptance testing
4. ..... testing is the re-execution of some subset of tests that have already
been conducted to ensure that changes have not propagated side effects.
Ans. Regression testing
5. One of the fields on a form contains a text box which accepts numeric values
in the range of 18 to 25. Identify invalid equivalance class.
- [x] 17
- [ ] 19
- [ ] 24
- [ ] 21

## PERT and CPM
- [Video explanation of Float, Free float, and Independent Float](https://www.youtube.com/watch?v=bChK4u34RD8)
 
### TF (Total float)
Amount of time an activity can be delayed without delaying the project end date.
Total float, slack, and float are the same.  

### FF (Free float)
Amout of time an activity can be delayed without delaying early start of the
successor activity.  

### Diagram sample
![](./pert-cpm-diagram.png)  

# White box testing
Exercise different programming structures and data structures used in program.
## Aims
- To ensure all independent paths have been exercised atleast once
- To exercise logical decisions on true and false sides
- To exercise loop bounds and boundaries 
- To exercise data structures
  
## Basis path testing
Derive a logical complexity measure. Use it as a guide to find a set of basis paths.  
Flow graph, which is drawn to represent the control flow of program is helpful in calculating
complexity. Flow graph is distinct from flow chart as the former avoids details not related to control flow
  
Eg.  
![](./flow-graph.png)
  
Components of flow graph are circles, and arrows. 
  
Regions are areas bounded by edges
and nodes. Predicate nodes is a node containing a decision, where two or more edges
emerge.
  
## Cyclomatic complexity
Cyclomatic complexity is a measure of logical complexity and helps to determine
number of independent paths in any basis set of a program. Consequently, it
is an upper bound on number of test that must be conducted for full path coverage.
$$
V(G) = No.\ of\ regions \\
or \\
V(G) = E - N + 2 \\
or \\
V(G) = P + 1 \\
$$
  
## Deriving test cases
1. Draw flow graph
2. Determine cyclomatic complexity
3. Determine a basis set
4. Prepare test cases looking at basis set
  
## Control structure testing
- Condition testing
- Data flow testing
- Loop testing
  
## Testing metrics
- Statement coverage, branch converage: _higher for unit testing_, _lower for system testing_
- Coverage of requirements: _traceability matrix_
  
## Reliability
- Probability/frequency of failure-free operations in a given time duration. Can be specified as Mean time to failure (MTTF).
- Defect rate
- Defect removal efficiency: _measure team's ability_
  
## Size metrics
Lines of code, is an easy measure of size used in early times. But this is not
efficient and is platform dependant.
### Halstead measure
![](./halstead-measures.png)
  
## Q&A
1. Intent of black box testing is to exercise different programming structures and data
structures in the program.  
Ans. False  
2. The identification of basis set enables us to ensure that testing
is efficient  
Ans. True  
3. If all statements of code are executed, that means every decision is 
executed atleast once.  
Ans. False, Statement coverage does not imply branch coverage. But branch coverage implies statment coverage.  
4. User acceptance testing is an example of  
Ans. Black box testing  
5. If problem occurs in a system 6 months after it completely goes live, what
will be the approach for fixing that problem on a very urgent basis?  
Ans. Make sure to do a reasonable regression test before releasing the fix.  
  
# Maintenance
Software change is inevitable. 
![](./maintenance.png)  
Evolution is the stage where software is in operational use as new requirements are
proposed and implemented in the system.  
Servicing is the stage where software remains useful, changes are made only to
keep it operational. Eg. bug fixes. Changes are often done to keep up with
change in software's environment.  
Phase out refers to a time after which a software may be used, but is not maintained.  
![](./change-cycle.png)  
![](./change-graph.png)  
  
First stage of change implementation involves program understanding. Here readability
of program and documentation is significant.
  
## Types of maintenance
- Repair
- Adapt
- Enhance

1. Corrective
2. Adaptive
3. Perfective
4. Preventive: _predict_, _reduce future maintenance cost_
  
## System re-engineering (Preventive)
Re-writing part or all of a legacy system. Reduce risk and overall cost of maintenance.
![](./re-engineering.png)  
### Activities
1. Source code translation
2. Reverse engineering
3. Program structure improvement: _can be automated_
4. Program modularization
5. Data reengineering
  
Refactoring makes code more easy to understand, it is a continuous process.
Re-engineering usually takes place after a system has been maintained for some time,
and maintenance costs are increasing.
  
## Q&A
1. Software maintenance includes  
Ans.  
- [ ] Error correction
- [ ] Enhancements of capability
- [ ] Deletion of obsolute capabilities
- [x] All of the mentioned
  
2. The modification of the software to match changes in the ever changing environment
falls under which category of maintenance?  
Ans. Adaptive  
  
3. Process of generating analysis and design documents from code is known as  
Ans. Reverse engineering
  

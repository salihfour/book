# Application Development
- [Spring & Hibernate for Beginners (includes Spring Boot) on Udemy](https://www.udemy.com/share/101WHS3@tpPAfqPsaSIwM4E3ozl_gOKtLVu3CN4UxwMAuojr953RRXlosPUSKaVfC98t9_Rb/)
- [React - The Complete Guide (incl Hooks, React Router, Redux) on Udemy](https://www.udemy.com/share/101Way3@TGZRiVSFESQt5094nd_1t1vd4LgPEt-CcMWT8eIAM4XNRISmtG2BQZnvKtlfY2oG/)
- [Android Programming with Kotlin for Beginners Ebook by John Horton](https://amzn.eu/d/bWHvQV0)
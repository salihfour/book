> 2-9-2020 Wednesday
# Intro

Important unique charecteristics that are present in a data is called a pattern  
  
Classification may be performed on statistical properties of the date
  
Three important mathematical foundations of pattern classification are
1. Linear algebra
2. Probability theory
3. Optimisation theory
  
# What is a pattern?
Pattern is the structure or regularity present in the data. It is a vaguely
defined entity with a name. Examples are
- Fingerprint image
- Handwritten word
- Speech signal

# Pattern recognition
Pattern recognition assign an object or event (pattern) to on of several categories (classes)
  
It is the study of how machines can
1. Observe the environment
2. Learn to distinguish patterns of interest
3. Make reasonable decisions about categories of patterns
  
A pattern class is a collection of similar, but not necessarily identical objects
  
## Steps in pattern recognition
  
![](./steps.png)  

# Features and feature vector
The measurements used by model for decision making
  
If $l$ such features are used, they form feature vector in the $l$ dimensional
feature space
  
Too strict features result in over fitting, whereas too generic features result in
under fitting.
  
![](./fitting.png)
  
# Learning and adapting
  
A model should learn and adapt
  
Learning: Some form of algorithm for reducing error on a set of training data
  
Adapt: The model should be capable of adjusting to certain changes in the 
environment
  
# Types of learning
  
Supervised (classification): a category label is provided for each element
in the training set
  
Unsupervised (clustering): based on similarity in data, the model extracts
natural clusters
  
Reinforced learning: algorithms learn to react to environment on their own
by generating feedback telling whether the decision is right or wrong
  
# To learn
  
![](./to-learn.png)
  
# Statistical pattern recognition
  
Focus statistical properties of patterns. Such properties are generally expressed
in terms of probability densities.
  
![](./statistical.png)
  
> 7 Sep 2020
  
# Linear Algebra

The two properties that should be met for linearity are
1. Superposition
2. Homogeneity _Scalability_
  
Linear systems are important because
1. Many natural and man-made systems can be modeled as linear
2. Signals can be modeled as vectors living in appropriate vector spaces
3. Addition of signals result in meaningful signals
4. Vector spaces allow us to apply intuitions and tools from 3d geometry
  
Non-linear systems can be constructed or approximated by combining linear models
  
![](./linear.png)
  
# Vector Space
  
A vector space is a non-empty set of elements called vectors which is closed
under vector addition and scalar multiplication.
  
## Axioms
Closed under addition: If $u$ and $v$ are elements of a set, $u+v$ is also an 
element of that set.
  
Closed under scalar multiplication: If $u$ is element of a set, $cu$ is also
an element of that set where $c$ is a constant.
  
Addition is commutative and associative. There exist an addictive identity denoted
as $0$, and all elements $x$ has an additive inverse denoted as $-x$. 
  
Multiplication is associative and distributive. There exist multiplicative
identity, denoted as $1$.
  
## Subspace
If a subset of vector space is closed under addition and multiplication , 
is called a subspace.
  
# Linear combination
![](./linear-combination.png)
  
## Related terms
Linear span  
![](./linear-span.png)
  
Spanning set / Generating set  
![](./spanning-set.png)  
  
# Linear dependence
![](./linear-dependence.png)
  
# Basis of vector space
![](./basis.png)
  
Dimensionality: It is the number of vectors present in a basis of a vector space.
There are _finite_ and _infinite_ dimensional vector spaces.
  
# Inner product  
![](./inner-product.png)  
  
## Inner product space
![](./inner-product-space.png)

## Standard inner product
$$
\langle u, v \rangle = z^Tx = \sum_{i=1}^{N}x_i z_i
$$

## Norms
![](./l2-norm.png)
  
Inner product l2 norm in $\R^N$  
$ ||x||_2 = \sqrt{\langle x, x \rangle} $
  
l1 norm is the absolute sum of co-ordinates
  
l0 norm is the count of non-zero values (co-ordinates) in the vector
  
Angle between two vectors is defined as  
$ \cos{\theta} = \frac{\langle u, v \rangle}{||u|| ||v||} $
  
Projection  
![](./projection.png)
  
## Other terms
Orthonormal vectors, Orthonormal basis
  
# Linear transformation
![](./linear-transformation.png)

## Key points
Linear transformation (T), Image / Range of T, Kernel of T

Eigen value of a matrix, eigen vector, eigen space, computation of eigen values
and eigen vectors

# Similarity transformation
![](./similar-matrices.png)
  
If a matrix $A$ is similar to a diagonal matrix $D$. Then $A$ is said to be
diagonalizable. Also, $A^k = CD^kC^{-1} $ as inner multiplications $C^{-1}C$
results in identity matrices.

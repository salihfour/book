# Docker compose

Here is an sample docker compose setup. It has two containers - pihole and cloudflared.
Pihole is a dns that blocks advertising domains. Cloudflared is a service that
encrypts dns queries. The following compose setup runs a pihole container
to blocks ads and forward remaining queries to a clouflared container so that
those queries are encrypted.

```
version: "3"

services:
  cloudflared:
    container_name: cloudflared
    image: visibilityspots/cloudflared
    restart: unless-stopped
    networks:
      pihole_net:
        ipv4_address: 10.0.0.2

  pi-hole:
    container_name: pi-hole
    image: pihole/pihole
    restart: unless-stopped
    ports:
      - "85:80/tcp"
      - "53:53/tcp"
      - "53:53/udp"
    environment:
      - ServerIP=10.0.0.3
      - DNS1='10.0.0.2#5054'
      - DNS2=''
      - IPv6=false
      - TZ=CEST-2
      - DNSMASQ_LISTENING=all
      - WEBPASSWORD=admin
    networks:
      pihole_net:
        ipv4_address: 10.0.0.3
    dns:
      - 127.0.0.1
      - 1.1.1.1
    cap_add:
      - NET_ADMIN

networks:
  pihole_net:
    driver: bridge
    ipam:
     config:
       - subnet: 10.0.0.0/29
```

Source - [Visibilityspots](https://visibilityspots.org/dockerized-cloudflared-pi-hole.html)

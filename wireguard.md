# Wireguard
Virtual ethernet

## Example: Simple ethernet to VM
Client side wg-quick config
```
[Interface]
Address = 10.128.1.3/24
PrivateKey = CLIENT_PRIVATE_KEY
ListenPort = 83

[Peer]
AllowedIPs = 10.128.1.100/32
PublicKey = SERVER_PUBLIC_KEY
Endpoint = SERVER_IP_ADDRESS
PersistentKeepalive = 25
```

Server config
```
[Interface]
Address = 10.128.1.100/24
PrivateKey = SERVER_PRIVATE_KEY
ListenPort = 82

[Peer]
AllowedIPs = 10.128.1.3/32
PublicKey = CLIENT_PUBLIC_KEY
PersistentKeepalive = 25
```

## Example: Proxy through wireguard
Client side confguration for proxying  through wireguard.
```
[Interface]
Address = 10.128.1.2/24
PrivateKey = CLIENT_PRIVATE_KEY
ListenPort = 83

[Peer]
AllowedIPs = 0.0.0.0/0
PublicKey = SERVER_PUBLIC_KEY
Endpoint = SERVER_IP_ADDRESS
PersistentKeepalive = 20
```
  
Corresponding server setup
```
[Interface]
Address = 10.128.1.100/24
PrivateKey = SERVER_PRIVATE_KEY
ListenPort = 82
PostUp = iptables -A FORWARD -i %i -j ACCEPT; iptables -t nat -A POSTROUTING -o SERVER_NETWORK_ADAPTER -j MASQUERADE
PostDown = iptables -D FORWARD -i %i -j ACCEPT; iptables -t nat -D POSTROUTING -o SERVER_NETWORK_ADAPTER -j MASQUERADE

[Peer]
AllowedIPs = 10.128.1.2/32
PublicKey = CLIENT_PUBLIC_KEY
```
  
## Note
Replace the following placeholders in above configs
```
CLIENT_PRIVATE_KEY
SERVER_PUBLIC_KEY
SERVER_PRIVATE_KEY
CLIENT_PUBLIC_KEY
SERVER_NETWORK_ADAPTER: Eg. eth0
```
- [Personal Wiki](/)
- [Coding Workflow](/coding-worklow.md)
- Self Hosting Reference
  - [Pihole](/docker/compose.md)
  - [Web Download Manager](/homelab/webui-aria2.md)

- [Language Catalog](/language-catalog.md)

- [Tools Catalog](/tools.md)

- [Interesting Topics](/interesting-topics.md)

- Network
  - [Wireguard](/wireguard.md)

- GNU/Linux

  - [Bash](/linux/bash.md)
  - [Backup](/linux/backup.md)
  - [Dev Tools](/linux/devtools.md)
  - [Automation](/linux/automation.md)
  - [Xmonad](/linux/xmonad.md)
  - [Polybar](/linux/polybar.md)
  - [Neovim](/linux/neovim.md)
  - [Troubleshoot](/linux/trouble.md)
  - [fzf](/linux/fzf.md)

- Platforms
  - [Gitlab](/gitlab)

- [Utility](/utility.md)
  - [Feeds](/feeds.md)

- Programming Languages
  - [Go](/programming-languages/go/)
  - [Haskell](/programming-languages/haskell)

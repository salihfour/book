# Loss Functions
## Classification
### Cross Entropy Loss
> $-\sum_{c=1}^My_{o,c}\log(p_{o,c})$  

M - number of classes (dog, cat, fish)  
log - the natural log  
y - binary indicator (0 or 1) if class label c
is the correct classification for observation o  
p - predicted probability observation o
is of class c  

## Regression
### L1 Loss
> $\sum | y_{true} - y_{pred} |$

### L2 Loss
> $\sum (y_{true} - y_{pred})^2$

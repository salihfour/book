# Aria2 Web

Linux Package `aria2`

### Config

_~/config/aria2/aria2.conf_

```
dir=/home/shares/public/aria2-downloads
rpc-secret=SECRET_PASSWORD
```

### Systemd service

_~/config/aria2/session.lock_

```

```

_/etc/systemd/user/aria2.service_

```
[Unit]
Description=Aria2 Service
After=network.target

[Service]
ExecStart=/usr/bin/aria2c --enable-rpc --rpc-listen-all --rpc-allow-origin-all --save-session %h/.config/aria2/session.lock --input-file %h/.config/aria2/session.lock --conf-path=%h/.config/aria2/aria2.conf

[Install]
WantedBy=default.target
```

### Web UI

Download UI source from (here)[https://github.com/ziahamza/webui-aria2]

### Nginx

```
server {
  listen 80;
  server_name SERVER_NAME;
  location / {
        root WEBUI_SOURCE_PATH/docs;
  }
}
```

## Reference

- [Web UI](https://github.com/ziahamza/webui-aria2)
- [Setting up](https://www.junian.net/tech/raspberry-pi-aria2-download-manager/)
- [Aria2 Systemd service](https://github.com/gutenye/systemd-units/tree/master/aria2)

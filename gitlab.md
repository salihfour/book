# Gitlab

## CI/CD
Gitlab CI/CD can be configured using `.gitlab-ci.yml` file. It automates tasks like build, test, and deploy. The following is a working
example that builds a website and serve it via gitlab pages. The website is built using node.  
_.gitlab-ci.yml_
```yml
stages:
 - deploy

pages:
    image: 'node:10'
    only: 
     - master
    stage: deploy
    script:
        - npm install
        - npm run build
        - mkdir .public
        - cp -r * .public
        - mv .public public
    artifacts:
        paths:
         - public
```

# Automation

## Crontab
Simple Job Scheduler. Check out [crontab generator](https://crontab-generator.org/)

## Systemd services
- Execute a service upon completion of another - [An example for troubleshooting
pulseaudio sound when laptop wakes up from suspended state](https://wiki.archlinux.org/index.php/PulseAudio/Troubleshooting#No_sound_after_resume_from_suspend)

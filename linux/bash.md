# Bash scripting

## Pass arguments
- `$n` for nth argument
- `$@` for all arguments
- Arguments containing spaces seems to get split into multiple arguments 
when forwarding arguments of a given script 
to a function within the script using `$@`, so I pass them individually as 
`func_call "$1" "$2"...` instead.
- Pattern matching - `[[ "string1" == "string2" ]]`
- Substring matching - `[[ "$sub" == *"sub"* ]]` 

## Loop
Sample
```
dirs=(
"foo"
"bar"
)

IFS='\n' # Only newlines are to be considered as delimiter
for d in ${dirs[@]}; do
    # do something
done
```

## Redirect output
- To run a command and ignore its log output and errors, use forward to 
/dev/null as follows `COMMAND > /dev/nul 2>&1`

## Conditionals
- `if [ -f ${x} ]` block runs if file specified by path `x` exists
- `if [ -z ${x} ]` block runs if variable `x` is not empty

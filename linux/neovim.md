# Usefull Plugins
## HTML
[Become a Html Ninja with Emmet for Vim](https://medium.com/vim-drops/be-a-html-ninja-with-emmet-for-vim-feee15447ef1)

Write `html:5` and use key combination `Ctrl-y` followed by `,` to generate html5 boilerplate

## Git

[Fugitive](https://github.com/tpope/vim-fugitive) is a powerful git plugin for
vim. I use it for usual git activities such as staging files, reviewing staged
changes, commit and push within the neovim interface. Its commands are very 
much similar to normal git commands and docs can be easily referred
in `:h fugitive`. So I don't have to put it here.  

Fugitive's `:Git push` may not work out of the box, refer
[FAQ](https://github.com/tpope/vim-fugitive) for details.

## Cool Tips
### Multiline editing
Go into box selection mode with `Esc` and `Ctrl-v`. Use up / down arrow keys
to select lines. Go into insert mode `Shift+i`, insert some charecter. Press 
`Esc` and the changes will be reflected in all selected lines. 
### Recording
1. Use command `qa` to start recording actions in `a` 
(`a` is choosen arbitrarily). Stop recording using command `q`
2. Repeat a recorded action `a` 10 times with the command `10@a`
  
### Resizing panes
- `Ctrl + w` followed by an optional number followed by `>` or `<` will change width
- `Ctrl + w` followed by an optional number followed by `+` or `-` will change width

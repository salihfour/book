# entr
Observe a file and run a command upon receiving triggers like *save*. Refer `man
entr` for details
### Use case
- Automatically compile and run code upon saving a source file

# FZF
A fuzzy file finder
  
## Notable features
- Very fast, search is asynchronous
- Fuzzy search to shortlist files
- Interactive commandline interface to pick one or more files
- Create an interactive menu from custom input
- Multiselect feature with an optional upper limit
- Customize look and feel of file picker
- A Preview window for each file which can be populated by a custom command like `cat`
- Set key/event bindings

## Notes
- Use `FZF_DEFAULT_OPTS` env var to save options
- Use `FZF_DEFAULT_COMMAND` env var to specifiy a default input like `fd --type f`

## Things to clarify
- How to automatically select the only match
- What is extended matching
- Filter mode
- Field index expressions


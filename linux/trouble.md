# Sound
## No sound after waking from suspended state (Pulseaudio)
Run `pulseaudio -k && pulseaudio --start`. As a side effect, this might close
some running applications.

# Xmonad

Xmonad is a tiling window manager for X window system. Configuration of Xmonad 
is in haskell language, consequently xmonad is highly programmable.

## Boilerplate

If a display manager is installed, we can add a desktop entry for xmonad. To do that, create a file with the following content.
`/usr/local/share/xsessions/xmonad.desktop`
```desktop
[Desktop Entry]
Name=xmonad
Comment=haskell based tiling wm
Exec=xmonad
TryExec=xmonad
Type=Application
X-LightDM-DesktopName=xmonad
DesktopNames=xmonad
Keywords=tiling;wm;windowmanager;window;manager;haskell
```
Once it is done, Xmonad will show up as a choice in login screen.

### Config
`~/.xmonad/xmonad.hs`
```haskell
import XMonad    
import XMonad.Hooks.DynamicLog    
import XMonad.Hooks.ManageDocks    
import XMonad.Util.Run(spawnPipe)    
import XMonad.Util.EZConfig(additionalKeys, removeKeys)    
import System.IO    
import System.Exit    
    
winM = mod4Mask    
altM = mod1Mask    
    
main = do    
    xmproc <- spawnPipe "xmobar"    
    
    xmonad $ defaultConfig    
        { manageHook = manageDocks <+> manageHook defaultConfig    
        , layoutHook = avoidStruts  $  layoutHook defaultConfig    
        , logHook = dynamicLogWithPP xmobarPP    
                        { ppOutput = hPutStrLn xmproc    
                        , ppTitle = xmobarColor "green" "" . shorten 50    
                        }    
        , modMask = mod4Mask     -- Rebind Mod to the Windows key    
        , terminal = "konsole"    
        } `removeKeys`    
        [ (mod4Mask .|. shiftMask, xK_q)    
        ] `additionalKeys`    
        [ ((winM .|. shiftMask, xK_z), spawn "xscreensaver-command -lock; xset dpms force off")    
        , ((winM .|. shiftMask, xK_e), io (exitWith ExitSuccess))     
        , ((winM .|. altM, xK_r), spawn "pulseaudio-ctl up")    
        , ((winM .|. altM, xK_l), spawn "pulseaudio-ctl down")    
        ]
```

Above config sets konsole as default terminal, and adds keybindings to switch off screen and control volume.
Refer [tips on configuring xmonad](https://wiki.haskell.org/Xmonad/General_xmonad.hs_config_tips) to customize xmonad.

# Backup Options
## tar & gpg
Create an encrypted archive of needed files. Preserve their file permissions.
```sh
# Example: Backing up ssh credentials
cd ~/
tar -cpvf - .ssh | gpg --symmetric --cipher-algo aes256 -o creds.tar.gpg

# Restoring the backup
cd ~/
gpg -d creds.tar.gpg | tar -xvf -
```

## Borgbackup
### Automating backups
Refer [Automating backups](https://borgbackup.readthedocs.io/en/stable/quickstart.html#automating-backups)
  
Backup script
```
#!/bin/sh

# Setting this, so the repo does not need to be given on the commandline:
export BORG_REPO=borg_backup_server:~/backup-hp

# See the section "Passphrase notes" for more infos.
export BORG_PASSPHRASE="$(pass show backup-hp@borgrepo@pi)"

# some helpers and error handling:
info() { printf "\n%s %s\n\n" "$( date )" "$*" >&2; }
trap 'echo $( date ) Backup interrupted >&2; exit 2' INT TERM

info "Starting backup"

# Backup the most important directories into an archive named after
# the machine this script is currently running on:

sudo --preserve-env=BORG_REPO,BORG_PASSPHRASE borg create                         \
    --verbose                       \
    --filter AME                    \
    --list                          \
    --stats                         \
    --show-rc                       \
    --compression lz4               \
    --exclude-caches                \
    --exclude '/home/*/.cache/*'    \
    --exclude '/var/tmp/*'          \
                                    \
    ::'{hostname}-{now}'            \
    /etc                            \
    /home                           \
    /root                           \
    /var                            \

backup_exit=$?

info "Pruning repository"

# Use the `prune` subcommand to maintain 7 daily, 4 weekly and 6 monthly
# archives of THIS machine. The '{hostname}-' prefix is very important to
# limit prune's operation to this machine's archives and not apply to
# other machines' archives also:

sudo --preserve-env=BORG_REPO,BORG_PASSPHRASE borg prune                          \
    --list                          \
    --prefix '{hostname}-'          \
    --show-rc                       \
    --keep-daily    7               \
    --keep-weekly   4               \
    --keep-monthly  6               \

prune_exit=$?

# use highest exit code as global exit code
global_exit=$(( backup_exit > prune_exit ? backup_exit : prune_exit ))

if [ ${global_exit} -eq 0 ]; then
    info "Backup and Prune finished successfully"
elif [ ${global_exit} -eq 1 ]; then
    info "Backup and/or Prune finished with warnings"
else
    info "Backup and/or Prune finished with errors"
fi

exit ${global_exit}
```
  
Mounting backup
```
#!/bin/sh
BORG_PASSPHRASE="$(pass show backup-hp@borgrepo@pi)" borg mount borg_backup_server:~/backup-hp ~/mnt
```
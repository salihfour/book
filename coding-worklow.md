# Coding Workflow

I use Git as VCS for coding. Git supports signing commits by a GPG key. GPG keys can be added to both Github and Gitlab account, it will verify your signed commits and indicate so. GPG keys can be managed by GnuPG cli (gpg). GnuPG come packaged in linux, it is also available in Git Bash for windows. GPG keys can be backed up using the cli's backup export option. For safety, encrypt the backup file with a long passphrase. gpg cli's cypher option can be used for encrypting files with passphrase. Store the backup file and passphrase securely at different locations.
  
I use IntelliJ IDEA for java projects. Projects opened in IntelliJ IDEA can be configured to sign commits using a PGP key that is added through gpg cli in Git Bash.
  
- [Signing commits in IntelliJ IDEA for Windows](https://www.jetbrains.com/help/idea/set-up-GPG-commit-signing.html#set-up-gpg-support-win)
- [Backing up keys managed by GPG](https://unix.stackexchange.com/a/482559)
- [Signing commits in Git](https://docs.github.com/en/authentication/managing-commit-signature-verification/signing-commits)
- [Telling Git about your GPG Key](https://docs.github.com/en/authentication/managing-commit-signature-verification/telling-git-about-your-signing-key#telling-git-about-your-gpg-key-1)
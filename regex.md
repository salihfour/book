# Extended regular expression

[Try out regex here](https://regexr.com/)

| Description               | Matcher   |
|---------------------------|---------  |
| word charecter            | \w        |
| not word                  | \W        |
| digit                     | \d        |
| not digit                 | \D        |
| whitespace                | \s        |
| not whitespace            | \S        |
| one of any                | [abc..]   |
| negated set               | [^abc]    |
| range                     | [a-z]     |
| word boundary             | \b        |
| not word boundary         | \B        |
| start, end                | `^`, `$`  |
| lookahead                 | (?=ABC)   |
| negetive lookahead        | (?=!ABC)  |
  
## Lazy matching
By default, quantifiers do a greedy match, i.e they will take the longest match.
To get the shortest match, use the `?` modifier. Eg. `^a.*?b` will match `accccb`
in the sentence `accccbccccb`. Whereas without `?`, the expression will match the 
whole sentence.

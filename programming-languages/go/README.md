# Go Lang

Developed by Google, Go lang stress on hiding complex design under a simple syntax. Tools like docker and kubernetes are built on top of Go. Unlike most programming languages, Go does not aim at continually extendind itself to cover all sorts of programming paradigms. Go aims at providing a base set of features to reduce the choices available for programmers to attain a common goal, that way programs written by distinct authors would look similar. Go aims at readablity and consistency.
  
[Docs](./https://golang.org/doc/)

## Syntax
The following notes are taken from [Go tour](https://tour.golang.org/welcome/1).
My syntax notes are not complete. The basics section in the tour describes all
syntax and is very consise. 
I found myself copy-pasting much of the notes from there to here. I decided to link 
to the tour instead of taking more notes.   
[Quick reference / Tour](https://tour.golang.org/welcome/1)

### Special Notes
- Unlike c/c++ ,`break` statement is not needed in any `switch` cases in golang.
However, with that as a motive do not expect that such a break statement can
be used to break out of a parent loop. For example, the following code will result in an infinite loop.
```
for {
    switch {
    case true:
        break
    }
}
```
The concept of labels can be used in this case to break out of the outer loop.
The following code does terminate
```
Outerloop:
    for {
        switch {
        case true:
            break Outerloop
        }
    }
```
Refer [Break statment - Go spec](https://golang.org/ref/spec#Break_statements) for more details

### Loop
Go has a single looping construct, which is `for`. 
```
for i := 0; i < 10; i++ {
    sum += i
}
```
When its init and post expressions, which are optional are avoided, it mimics `while` loop.  
```
for ; sum < 1000; {
    sum += sum
}
```
At this point, the semicolon can be safely discarded
```
for sum < 1000 {
    sum += sum
}
```
Omit the loop condition and it loops forever
```
for {
}
```

### If-else
Sample code
```
if v := math.Pow(x, n); v < lim {
    return v
} else {
    fmt.Printf("%g >= %g\n", v, lim)
}
```
The statement between if keyword and comparator is called a short statement. It
is optional. Variables declared there are within the scop of if and its else
block.

### Switch
Sample code
```
switch os := runtime.GOOS; os {
case "darwin":
    fmt.Println("OS X.")
case "linux":
    fmt.Println("Linux.")
default:
    // freebsd, openbsd,
    // plan9, windows...
    fmt.Printf("%s.\n", os)
}
```
Go's switch cases need not be constants, 
and the values involved need not be integers.
  
Switch without a condition is the same as switch true. 
This construct can be a clean way to write long if-then-else chains.
```
switch {
case t.Hour() < 12:
    fmt.Println("Good morning!")
case t.Hour() < 17:
    fmt.Println("Good afternoon.")
default:
    fmt.Println("Good evening.")
}
```

### Defer
 A defer statement defers the execution of a function until the surrounding function returns.

The deferred call's arguments are evaluated immediately, but the function call is not executed until the surrounding function returns
```
func main() {
	defer fmt.Println("world")

	fmt.Println("hello")
}
```
Deferred function calls are pushed onto a stack. When a function returns, its deferred calls are executed in last-in-first-out order.

### Struct
Sample
```
type Vertex struct {
	X int
	Y int
}
func main() {
    var (
        v1 = Vertex{1, 2}  // has type Vertex
        v2 = Vertex{X: 1}  // Y:0 is implicit
        v3 = Vertex{}      // X:0 and Y:0
        p  = &Vertex{1, 2} // has type *Vertex
    )
	v.X = 4 // Access field X
	fmt.Println(v.X)
}
```
To access the field X of a struct when we have the struct pointer p we could 
write (\*p).X. However, that notation is cumbersome, so the language 
permits us instead to write just p.X, without the explicit dereference.

### Array
The type `[n]T` is an array of `n` values of type `T`.  
Declarations
```
var a [2]string
a[0] = "Hello"
a[1] = "World"
primes := [6]int{2, 3, 5, 7, 11, 13}
```
A whole array can be printed by avoiding index.

### Slices
Selects a half-open range which includes the first element, but excludes the last one.
```
primes := [6]int{2, 3, 5, 7, 11, 13}
var s []int = primes[1:6]
```
Slice are referers to original array. It is not a duplicate copy.  
Both lower and upper bound can be omitted when slicing to slice at the extremes.  
The length and capacity of a slice s can be obtained using the expressions len(s) and cap(s).  
 The length of a slice is the number of elements it contains.  
The capacity of a slice is the number of elements in the underlying array, 
counting from the first element in the slice.  
 The zero value of a slice is nil. A nil slice has a length and capacity of 
 0 and has no underlying array.  
  
> For the rest of syntax lessons, refer [Quick reference / Go Tour](https://tour.golang.org/welcome/1)

## Error handling
- [Watch youtube video](https://www.youtube.com/watch?v=MSDvGsJQ-KI)

## Workspace
Watch [Writing, building, installing, and testing Go code](https://www.youtube.com/watch?v=XCsL89YtqCs)  

Assign a path to environment variable `GOPATH` and go will treat it as the workspace
in which everything runs. For convenience in running compiled binaries, `$GOPATH/bin` can
be added to `PATH`.  
- `go build` compiles packages and discard their output. Use it to test for errors.
- `go install` compiles packages and saves packages and binaries into the workspace.
- `go test` compiles all packages including the files that end with `_test`
- Tests for package in file `pkgname.go` can be written in `pkgname_test.go`. Public
functions in such file will be executed when running `go test`
- A package can have multiple files. Files for a given package is supposed to reside in a dedicated directory.
Subdirectories can be made to nest packages.

## Troubleshoot
1. `go get golang.org/x/tour` downloaded `tour` binary to the folder `$HOME/go/bin`.
This binary is not in PATH. So I had to 
`export GOPATH="$HOME/go"` and append `$GOPATH/bin` to PATH, in order to 
run the binary.

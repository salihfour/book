# Haskell

A statically typed purely functional programming language with type inference, 
supports parallel programming.

[Video tutorial](https://www.youtube.com/watch?v=02_H3LjqMr8)  
[Cheat Sheet](http://www.newthinktank.com/2015/08/learn-haskell-one-video/)  

# Interactive prompt
- `ghci` to start interactive prompt
- `:h` for help
- `:t functionName` to see a functions type

# Syntax notes

- Negetive numbers has to be wrapped in `()`
- `--` starts a single line comment
- Expression `2 : 8 : [3, 5]` evaluates to list `[2, 8, 3, 5]`
- List indexing - `listname !! index`, e.g. `primes !! 2`
- Lambda function => `(\x -> x * 2)` 

## Conditionals
Copied from [here](http://www.newthinktank.com/2015/08/learn-haskell-one-video/)
```hs
-- Every if statement must contain an else
doubleEvenNumber y = 
	if (y `mod` 2 /= 0)
		then y
		else y * 2

-- We can use case statements 
getClass :: Int -> String
getClass n = case n of
	5 -> "Go to Kindergarten"
	6 -> "Go to elementary school"
	_ -> "Go some place else"
```

## List generation
```
[1..10]
[2,4..20]
['A','C'..'Z']
[10,20..]                       --Infinite list  
[x * 2 | x <- [1..10]]          --Returns the list [2,4..20]  
[x * 2 | x <- [1..10], x * 2 < 15, x > 3]
```

## Function definition
The following code defines a factorial function recursively. Passed parameter is compared
with function definitions on line 2 and 3 in order.   
```
factorial :: Int -> Int
factorial 0 = 1
factorial n = n * factorial (n - 1)
```
Unlike haskell, this behaviour requires use of
conditionals like if-else or switch-case in oop languages.
  
## Misc
`x@(a:b)` accepts an list aliasing it as x, with a as first item and b as the rest of list.
For e.g  
```haskell interactive prompt
Prelude> x@(a:bc) = [1,2,3]
Prelude> x
[1,2,3]
Prelude> a
1
Prelude> bc
[2,3]
Prelude>
```

# Functions

```
--List
sort []
length []
reverse []
null []                 --Returns true if list is empty
head []                 --First value
last []
take 3 []               --First 3 values
drop 3 []               --List excluding first 3 values
maximum []
minimum []
sum []
product []
repeat 2                --Infinite list of 2s
replicate 10 3          --List of length 10 containing 3s
cycle [1,2,3]           --Infinite list looping [1,2,3]
zipWith (+) [1,3] [2, 3] --Evaluates to [3, 6]
filter (>5) []
takeWhile (<=20) [1,2..]
foldl (+) [1,2,3]       --Returns 6
foldr (*) [1,2,3]
zip [] []               --Returns list of tuples whose values are extracted from two lists

--Math
pi
exp 9       --e^9
log 9       --log9
squared 9
truncate 9.99
round 9.99
ceiling 9.99
floor 9.99
```

# 

# Operators
```
==
/=      --Not equal
||
&&
not(True)
++          --Concat
^           --Power
```
